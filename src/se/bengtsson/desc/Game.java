package se.bengtsson.desc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.Random;

import se.bengtsson.desc.creatures.Player;
import se.bengtsson.desc.dungeon.Maps;
import se.bengtsson.desc.screen.Animation;
import se.bengtsson.desc.screen.DaCube;
import se.bengtsson.desc.screen.HelpView;
import se.bengtsson.desc.screen.InventoryView;
import se.bengtsson.desc.screen.MainView;

/**
 * Game class, creates and handles the game and user input
 * 
 * @author Marcus Bengtsson
 * 
 */
public class Game implements Serializable {

	private static final long serialVersionUID = 256647161953803506L;
	private Player player;
	private MainView mainView;
	private Maps map;
	private Enemies enemies;
	private int area;

	private int turn = 1;

	private String command = "";
	private String newCommand = "";

	private String output;
	private Game loadedGame;

	private boolean halt = false;
	private boolean classic = false;

	/**
	 * Constructor, sets up a new game
	 */
	public Game() {

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		String name = "";

		System.out.println("Starting a new game");
		do {

			System.out.print("enter your name (max 12 chars)> ");
			System.out.flush();

			try {
				name = input.readLine();
			} catch (IOException e) {
				System.out.println("Something went wrong.");
			}

		} while (name.length() > 12 || name.equals(""));

		System.out.print("\r\nDo you want to play in classic mode with permadeath enabled, \r\nsaving game is disbled except when quiting game and \r\nclassic saves are deleted when loaded (y/N)> ");
		System.out.flush();
		String mode = "";
		
		try {
			mode = input.readLine();
		} catch (IOException e) {
			System.out.println("Something went wrong.");
		}
		
		if (mode.toLowerCase().equals("y") || mode.toLowerCase().equals("yes")){
			classic = true;
		}
		
		area = 1;

		player = new Player(1, name, '@');

		mainView = new MainView(player);
		map = new Maps(area);

		player.setRandomLocation(map);

		enemies = new Enemies(map);

		Random rnd = new Random();
		enemies.addEnemies(rnd.nextInt(5) + 5, 0, player.getLevel());
		
		map.addCreature(player);
		mainView.setMap(map.getVisibleMap());
		
		if(classic) {
			mainView.addTitle("DUNGEON ESCAPE - CLASSIC MODE");
		}

	}

	/**
	 * This method handles one game-turn
	 *  
	 * @return the games state after a completed turn
	 */
	public Game nextTurn() {

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));


		
		mainView.addString(62, 1, "Turn: " + turn);
		mainView.addString(61, 24, "Area:         " + area);
		mainView.addString(61, 25, "                 ");
		mainView.addString(61, 25, "Enemies left: " + enemies.size());

		mainView.addStats(player);
		mainView.setMap(map.getVisibleMap());
		mainView.printScr();

		System.out.print("Enter command> ");
		System.out.flush();

		try {
			newCommand = input.readLine();
		} catch (IOException e) {
			System.out.println("Input error");
			
		}

		if (!newCommand.equals("")) {
			command = newCommand;
		}

		output = commands(command);
		mainView.message(output);

		if(!isHalted()) {
			moveEnemies();
		}
			
		if (player.getCurrentHp() < player.getTotalHp()) {
			player.setCurrentHp(player.getCurrentHp() + 1);
		}
		if (player.getCurrentPwr() < player.getTotalPwr()) {
			player.setCurrentPwr(player.getCurrentPwr() + 1);
		}

		if (player.levelUp()) {
			enemies.levelUp();
		}

		turn++;

		if (loadedGame == null) {
			return this;
		} else {
			return loadedGame;
		}
	}

	/**
	 * Game over!
	 * Plays an annimation and halts the game
	 */
	private void gameOver() {
		Animation anim = new Animation(1);
		anim.animateUp(100);

		halt = true;
	}

	/**
	 * Handles commands given by the player
	 * 
	 * @param command
	 *            command to handle
	 * @return string to show in the message area
	 */
	private String commands(String command) {

		String output = "";

		String direction = "";

		
		if(command.toLowerCase().equals("n")){
			command = "north";
		}else if(command.toLowerCase().equals("s")){
			command = "south";
		}else if(command.toLowerCase().equals("w")){
			command = "west";
		}else if(command.toLowerCase().equals("e")){
			command = "east";
		}else if (command.toLowerCase().equals("inventory")) {
			command = "inv";
		}
		
		if (command.toLowerCase().equals("north") || command.toLowerCase().equals("south") || command.toLowerCase().equals("west") || command.toLowerCase().equals("east")) {
			direction = command;
			command = "move";
		}

		switch (command.toLowerCase()) {
		case "wait":

			output = "waiting";

		case "move":

			int collision = player.move(map, direction);

			if (collision == 0) {
				output = "moved " + direction;
			} else if (collision == 1) {
				output = "there's a wall";
			} else if (collision == 2) {
				Fight fight = new Fight(player, enemies.getEnemyAt(player.getX(), player.getY()));

				if (fight.startFight()) {
					player.loot(enemies.getEnemyAt(player.getX(), player.getY()));
					output = "You won";
					enemies.removeEnemy(enemies.getEnemyAt(player.getX(), player.getY()));
				} else {

					gameOver();
				}
			} else if (collision == 4) {
				if (enemies.size() == 0) {
					nextLevel();
					output = "new area: " + area;
				} else {
					output = "locked";
				}
			}

			break;
		case "inv":

			InventoryView inv = new InventoryView(player);
			inv.handleInventory();

			break;
		case "load":

			FileManager loadManager = new FileManager();
			loadedGame = loadManager.loadGame();
			
			if(loadedGame == null){
				output = "nothing loaded";
			}

			break;
		case "save":
			
			if(classic) {
				output = ("not allowed");
				break;
			}

			FileManager saveManager = new FileManager();
			saveManager.saveGame(this, player);

			output = "game saved";

			break;
		case "spin":

			DaCube cube = new DaCube();
			cube.makeItSpin(30);

			break;
		case "help":

			HelpView help = new HelpView();
			help.showHelp();

			break;
		case "exit":
			System.out.println("Bye Bye, " + player.getName());
						
			if(classic) {
				FileManager fm = new FileManager();
				fm.saveGame(this, player);
			}
			halt = true;
			
			break;
		default:
			output = "can't do that";
		}

		map.setArea(map.getArea());
		map.addCreature(player);

		return output;
	}

	/**
	 * Sets up next level, loads map and adds npcs
	 */
	private void nextLevel() {

		area++;

		map.setArea(area);
		
		player.setRandomLocation(map);
		enemies = new Enemies(map);
		Random rnd = new Random();
		enemies.addEnemies(rnd.nextInt(5) + 5, rnd.nextInt(2) + 1, player.getLevel());

		map.addCreature(player);
		mainView.setMap(map.getMap());

	}

	/**
	 * Returns true if the game is halted
	 * 
	 * @return true if halted
	 */
	public boolean isHalted() {
		return halt;
	}
	
	/**
	 * Check if game is run in classic mode
	 * 
	 * @return true if game is in classic mode
	 */
	public boolean isClassic() {
		return classic;
	}

	/**
	 * Moves the npcs and starts a fight if they are colliding
	 * with the player 
	 */
	private void moveEnemies() {

		for (int i = 0; i < enemies.size(); i++) {
			if (enemies.getEnemy(i).moveNpc(map, player)) {
				Fight fight = new Fight(enemies.getEnemy(i), player);

				if (fight.startFight()) {
					gameOver();
				} else {
					player.loot(enemies.getEnemy(i));
					enemies.removeEnemy(enemies.getEnemy(i));
				}
			}
			
			map.addCreature(enemies.getEnemy(i));
		}
	}
}
