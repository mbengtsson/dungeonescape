package se.bengtsson.desc.items;

import java.io.Serializable;

/**
 * Abstract item class, all items inherits this class
 * 
 * @author Marcus Bengtsson
 * 
 */
public abstract class Item implements Serializable {

	private static final long serialVersionUID = 4408802490432185730L;
	private String name;
	private boolean weapon;
	private boolean armour;

	private int weight;
	private int itemStat;

	/**
	 * Creates a new item
	 * 
	 * @param name
	 *            item name
	 * @param weight
	 *            item weight
	 * @param type
	 *            type of item (1-weapon, 2-armour)
	 */
	public Item(String name, int weight, int type) {

		this.name = name;
		this.weight = weight;

		if (type == 1) {
			this.weapon = true;
		} else if (type == 2) {
			this.armour = true;
		}

	}

	/**
	 * Get item name
	 * 
	 * @return item name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Get item weight
	 * 
	 * @return item weight
	 */
	public int getWeight() {
		return weight;
	}

	/**
	 * get item stat (health or power modifier)
	 * 
	 * @return item stat
	 */
	public int getItemStat() {
		return itemStat;
	}

	/**
	 * sets item stat (health or power modifier)
	 * 
	 * @param itemStat
	 *            item stat
	 */
	protected void setItemStat(int itemStat) {
		this.itemStat = itemStat;
	}

	/**
	 * Get item type
	 * 
	 * @return 1 if weapon, 2 if armour
	 */
	public int getType() {
		if (weapon) {
			return 1;
		} else if (armour) {
			return 2;
		} else {
			return 0;
		}

	}

}
