package se.bengtsson.desc.items;

/**
 * Leather armour class, creates leather armour
 * 
 * @author Marcus Bengtsson
 * 
 */
public class LeatherArmour extends Item {

	private static final long serialVersionUID = 4905931527645585281L;

	/**
	 * Creates a new leather armour
	 */
	public LeatherArmour() {
		super("Leather", ItemStats.LEATHER_ARMOUR.getWeight(), 2);

		setItemStat(ItemStats.LEATHER_ARMOUR.getStatValue());
	}

}
