package se.bengtsson.desc.items;

/**
 * Axe class, creates axes
 * 
 * @author Marcus Bengtsson
 * 
 */
public class Axe extends Item {

	private static final long serialVersionUID = -3403862511467349019L;

	/**
	 * Creates a new axe
	 */
	public Axe() {
		super("Axe", ItemStats.AXE.getWeight(), 1);

		setItemStat(ItemStats.AXE.getStatValue());
	}

}
