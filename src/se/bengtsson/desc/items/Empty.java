package se.bengtsson.desc.items;

/**
 * Empty class, creates "empty items" that is representing an empty slot
 * 
 * @author Marcus Bengtsson
 * 
 */
public class Empty extends Item {

	private static final long serialVersionUID = -6640406409554267015L;

	/**
	 * Creates a new empty item to represent nothing slotted
	 */
	public Empty() {
		super("Empty", ItemStats.EMPTY.getWeight(), 0);

		setItemStat(ItemStats.EMPTY.getStatValue());
	}

}
