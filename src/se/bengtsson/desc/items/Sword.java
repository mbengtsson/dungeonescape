package se.bengtsson.desc.items;

/**
 * Sword class, creates swords
 * 
 * @author Marcus Bengtsson
 * 
 */
public class Sword extends Item {

	private static final long serialVersionUID = -8535998023684252747L;

	/**
	 * Creates a new sword
	 */
	public Sword() {
		super("Sword", ItemStats.SWORD.getWeight(), 1);

		setItemStat(ItemStats.SWORD.getStatValue());
	}

}
