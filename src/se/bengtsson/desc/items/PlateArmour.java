package se.bengtsson.desc.items;

/**
 * Plate armour class, creates plate armour
 * 
 * @author Marcus Bengtsson
 * 
 */
public class PlateArmour extends Item {

	private static final long serialVersionUID = 7359805453634949726L;

	/**
	 * Creates a new plate armour
	 */
	public PlateArmour() {
		super("Plate", ItemStats.PLATE_ARMOUR.getWeight(), 2);

		setItemStat(ItemStats.PLATE_ARMOUR.getStatValue());
	}

}
