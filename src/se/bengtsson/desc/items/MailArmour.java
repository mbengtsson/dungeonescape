package se.bengtsson.desc.items;

/**
 * Mail armour class, creates mail armour
 * 
 * @author Marcus Bengtsson
 * 
 */
public class MailArmour extends Item {

	private static final long serialVersionUID = -1568930202637414385L;

	/**
	 * Creates a new mail armour
	 */
	public MailArmour() {
		super("Mail", ItemStats.MAIL_ARMOUR.getWeight(), 2);

		setItemStat(ItemStats.MAIL_ARMOUR.getStatValue());
	}

}
