package se.bengtsson.desc.items;

/**
 * Club class, creates clubs
 * 
 * @author Marcus Bengtsson
 * 
 */
public class Club extends Item {

	private static final long serialVersionUID = -5339192027448888345L;

	/**
	 * Creates a new club
	 */
	public Club() {
		super("Club", ItemStats.CLUB.getWeight(), 1);

		setItemStat(ItemStats.CLUB.getStatValue());
	}

}
