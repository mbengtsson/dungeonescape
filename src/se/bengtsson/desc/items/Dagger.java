package se.bengtsson.desc.items;

/**
 * Dagger class, creates daggers
 * 
 * @author Marcus Bengtsson
 * 
 */
public class Dagger extends Item {

	private static final long serialVersionUID = -3876904660993737261L;

	/**
	 * Creates a new dagger
	 */
	public Dagger() {
		super("Dagger", ItemStats.DAGGER.getWeight(), 1);

		setItemStat(ItemStats.DAGGER.getStatValue());

	}

}
