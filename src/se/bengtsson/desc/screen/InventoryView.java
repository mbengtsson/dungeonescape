package se.bengtsson.desc.screen;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import se.bengtsson.desc.creatures.Player;
import se.bengtsson.desc.items.Item;

/**
 * Inventory view class, handles the inventory view
 * 
 * @author Marcus Bengtsson
 * 
 */
public class InventoryView extends Screen {

	private static final long serialVersionUID = 7050123741314901239L;
	private final String TITLE;
	private Player player;
	private boolean loop = true;

	/**
	 * Sets up the inventory view
	 * 
	 * @param player
	 *            this players inventory
	 */
	public InventoryView(Player player) {

		this.player = player;

		TITLE = "DUNGEON ESCAPE - " + this.player.getName().toUpperCase() + "S INVENTORY";

		addTitle(TITLE);
		addArea(2, 3, 25, 20, ' ');
		addString(10, 3, "[ARMOURS]");
		addArmours();

		addArea(30, 3, 25, 20, ' ');
		addString(38, 3, "[WEAPONS]");
		addWeapons();

		addString(4, 25, "Type equip <item> to equip an item. (eg. equip sword)");
		addString(4, 26, "Type \"back\" to return to main view.");
		addStats();

	}

	/**
	 * Handles the inventory
	 */
	public void handleInventory() {

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		String usrIn = "";

		while (loop) {
			printScr();

			System.out.print("Enter command> ");
			System.out.flush();
			try {
				usrIn = input.readLine();
			} catch (IOException e) {
				System.out.println("input error");
			}

			String[] command = usrIn.split(" ");

			commands(command);

			addArea(2, 3, 25, 20, ' ');
			addString(10, 3, "[ARMOURS]");
			addArmours();

			addArea(30, 3, 25, 20, ' ');
			addString(38, 3, "[WEAPONS]");
			addWeapons();

			addStats();
		}
	}

	/**
	 * Handles user commands for the inventory
	 * 
	 * @param command
	 *            user command
	 */
	private void commands(String[] command) {

		switch (command[0].toLowerCase()) {
		case "equip":
			if (command.length < 2) {
				break;
			}

			equipItem(player.getInv().removeItem(command[1]));

			break;
		case "back":
			loop = false;
		}

	}

	/**
	 * Shows armours from the inventory in the inventory view
	 */
	private void addArmours() {

		for (int i = 0; i < player.getInv().getArmours().size(); i++) {
			addString(4, i + 6, player.getInv().getArmours().get(i).getName() + " " + player.getInv().getArmours().get(i).getItemStat() + " " + "HP");
		}
	}

	/**
	 * Shows weapons from the inventory in the inventory view
	 */
	private void addWeapons() {

		for (int i = 0; i < player.getInv().getWeapons().size(); i++) {
			addString(32, i + 6, player.getInv().getWeapons().get(i).getName() + " " + player.getInv().getWeapons().get(i).getItemStat() + " " + "dmg");
		}
	}

	/**
	 * Shows players current stats
	 */
	private void addStats() {

		addString(61, 3, "<" + player.getName().toUpperCase() + ">");
		addString(61, 5, "Lvl: " + player.getLevel());
		addString(61, 6, "XP:  " + player.getXp());
		addString(61, 8, "                 ");
		addString(61, 8, "HP:  " + player.getCurrentHp() + " (" + player.getHp() + "+" + player.getArmour().getItemStat() + ")");
		addString(61, 9, "                 ");
		addString(61, 9, "Pwr: " + player.getCurrentPwr() + " (" + player.getPwr() + "+" + player.getWeapon().getItemStat() + ")");
		addString(61, 11, "Armour:");
		addString(61, 12, "                 ");
		addString(61, 12, player.getArmour().getName() + " +" + player.getArmour().getItemStat() + " HP");
		addString(61, 13, "Weapon:");
		addString(61, 14, "                 ");
		addString(61, 14, player.getWeapon().getName() + " +" + player.getWeapon().getItemStat() + " dmg");

	}

	/**
	 * Equip player with an item
	 * 
	 * @param item
	 *            to equip
	 * @return true if success, false if fail
	 */
	private boolean equipItem(Item item) {

		if (item == null) {
			return false;
		}

		if (item.getType() == 1) {
			if (!player.getWeapon().getName().equals("Empty")) {
				player.getInv().addItem(player.getWeapon());
			}
			player.setWeapon(item);
			return true;
		} else if (item.getType() == 2) {
			if (!player.getArmour().getName().equals("Empty")) {
				player.getInv().addItem(player.getArmour());
			}
			player.setArmour(item);
			return true;
		}

		return false;
	}

}
