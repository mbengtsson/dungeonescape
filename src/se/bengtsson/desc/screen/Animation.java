package se.bengtsson.desc.screen;

/**
 * Animation class, animates a 2d-char array up or down
 * 
 * @author Marcus Bengtsson
 * 
 */
public class Animation extends Screen {

	private static final long serialVersionUID = -5503772069231818448L;
	private char[][] text;
	private String title;

	/**
	 * Setting up an animation
	 * 
	 * @param anim
	 *            chose animation (1-game over, 2-splash-screen)
	 */
	public Animation(int anim) {

		text = new char[27][77];

		switch (anim) {
		case 1:
			setAnim1();
			break;
		case 2:
			setAnim2();
			break;
		}

		addTitle(title);
	}

	/**
	 * Animates downwards
	 * 
	 * @param speed
	 *            animation speed, lower number is faster
	 */
	public void animateDown(int speed) {

		int height = text.length - 1;
		int width = text[0].length;

		for (int i = 0; i < height; i++) {

			for (int y = 0; y < i; y++) {
				for (int x = 0; x < width; x++) {
					screen[y + 1][x + 1] = text[height - i + y][x];
				}
			}

			printScr();

			try {
				Thread.sleep(speed);
			} catch (InterruptedException e) {

			}
		}
	}

	/**
	 * Animates upwards
	 * 
	 * @param speed
	 *            animation speed, lower number is faster
	 */
	public void animateUp(int speed) {

		int height = text.length - 1;
		int width = text[0].length;

		for (int i = 0; i < height; i++) {

			for (int y = 0; y < i; y++) {
				for (int x = 0; x < width; x++) {
					screen[y + height - i][x + 1] = text[y][x];
				}
			}

			printScr();

			try {
				Thread.sleep(speed);
			} catch (InterruptedException e) {

			}
		}
	}

	/**
	 * Adds a string to a row in the text array
	 * 
	 * @param l
	 *            row to add
	 * @param s
	 *            string to add
	 */
	private void addLine(int l, String s) {

		for (int i = 0; i < s.length(); i++) {

			text[l][i] = s.charAt(i);

		}
	}

	/**
	 * Sets animation 1 (game over-screen)
	 */
	private void setAnim1() {

		title = "GAME OVER MAN, GAME OVER...";
		addLine(0, "                                                                             ");
		addLine(1, "                                                                             ");
		addLine(2, "                ___          ___          ___          ___                   ");
		addLine(3, "               /\\  \\        /\\  \\        /\\__\\        /\\  \\                  ");
		addLine(4, "              /::\\  \\      /::\\  \\      /::|  |      /::\\  \\                 ");
		addLine(5, "             /:/\\:\\  \\    /:/\\:\\  \\    /:|:|  |     /:/\\:\\  \\                ");
		addLine(6, "            /:/  \\:\\  \\  /::\\~\\:\\  \\  /:/|:|__|__  /::\\~\\:\\  \\               ");
		addLine(7, "           /:/__/_\\:\\__\\/:/\\:\\ \\:\\__\\/:/ |::::\\__\\/:/\\:\\ \\:\\__\\              ");
		addLine(8, "           \\:\\  /\\ \\/__/\\/__\\:\\/:/  /\\/__/~~/:/  /\\:\\~\\:\\ \\/__/              ");
		addLine(9, "            \\:\\ \\:\\__\\       \\::/  /       /:/  /  \\:\\ \\:\\__\\                ");
		addLine(10, "             \\:\\/:/  /       /:/  /       /:/  /    \\:\\ \\/__/                ");
		addLine(11, "              \\::/  /       /:/  /       /:/  /      \\:\\__\\                  ");
		addLine(12, "               \\/__/        \\/__/        \\/__/        \\/__/                  ");
		addLine(13, "                ___         ___          ___          ___                    ");
		addLine(14, "               /\\  \\       /\\__\\        /\\  \\        /\\  \\                   ");
		addLine(15, "              /::\\  \\     /:/  /       /::\\  \\      /::\\  \\                  ");
		addLine(16, "             /:/\\:\\  \\   /:/  /       /:/\\:\\  \\    /:/\\:\\  \\                 ");
		addLine(17, "            /:/  \\:\\  \\ /:/__/  ___  /::\\~\\:\\  \\  /::\\~\\:\\  \\                ");
		addLine(18, "           /:/__/ \\:\\__\\|:|  | /\\__\\/:/\\:\\ \\:\\__\\/:/\\:\\ \\:\\__\\               ");
		addLine(19, "           \\:\\  \\ /:/  /|:|  |/:/  /\\:\\~\\:\\ \\/__/\\/_|::\\/:/  /               ");
		addLine(20, "            \\:\\  /:/  / |:|__/:/  /  \\:\\ \\:\\__\\     |:|::/  /                ");
		addLine(21, "             \\:\\/:/  /   \\::::/__/    \\:\\ \\/__/     |:|\\/__/                 ");
		addLine(22, "              \\::/  /     ~~~~         \\:\\__\\       |:|  |                   ");
		addLine(23, "               \\/__/                    \\/__/        \\|__|                   ");
		addLine(24, "                                                                             ");
		addLine(25, "                                                                             ");
		addLine(26, "                                                                             ");

	}

	/**
	 * Sets animation 2 (splash-screen)
	 */
	private void setAnim2() {

		title = "DUNGEON ESCAPE";
		addLine(0, "                                                                             ");
		addLine(1, "          ________                                                           ");
		addLine(2, "          \\______ \\  __ __  ____    ____   ____  ____   ____                 ");
		addLine(3, "           |    |  \\|  |  \\/    \\  / ___\\_/ __ \\/  _ \\ /    \\                ");
		addLine(4, "           |    `   \\  |  /   |  \\/ /_/  >  ___(  <_> )   |  \\               ");
		addLine(5, "          /_______  /____/|___|  /\\___  / \\___  >____/|___|  /               ");
		addLine(6, "            ______\\/___        \\//_____/      \\/           \\/                ");
		addLine(7, "            \\_   _____/ ______ ____ _____  ______   ____                     ");
		addLine(8, "             |    __)_ /  ___// ___\\\\__  \\ \\____ \\_/ __ \\                    ");
		addLine(9, "             |        \\\\___ \\\\  \\___ / __ \\|  |_> >  ___/                    ");
		addLine(10, "            /_______  /____  >\\___  >____  /   __/ \\___  >                   ");
		addLine(11, "                    \\/     \\/     \\/     \\/|__|        \\/                    ");
		addLine(12, "                  A Rougelike by                                             ");
		addLine(13, "                    Marcus Bengtsson GBJU13                                  ");
		addLine(14, "            __  __  __  __                __  __  __  __                     ");
		addLine(15, "            ||\\_||\\_||\\_||\\               ||\\_||\\_||\\_||\\                    ");
		addLine(16, "            |>          <||               |>          <||                    ");
		addLine(17, "            |>  #    #  <|:\\|\\            |>  #    #  <|:\\|\\                 ");
		addLine(18, "            |>  #    #  <|:::| __  __  __ |>  #    #  <|:::|                 ");
		addLine(19, "            |>          <|_||\\_||\\_||\\_||\\|>          <|:::|                 ");
		addLine(20, "            |>  #    #                        #    #  <|:::|                 ");
		addLine(21, "            |>  #    #        ________        #    #  <|:::|                 ");
		addLine(22, "            |>               /||||||||\\               <|:::|                 ");
		addLine(23, "            |>               |++++++++|               <|:::|                 ");
		addLine(24, "            |>               ||||||||||               <|:::|                 ");
		addLine(25, "            ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                 ");
		addLine(26, "                                                                             ");
	}
}

/*
addLine( 0,"                                                                              ");
addLine( 1,"                                                                              ");
addLine( 2,"                                                                              ");
addLine( 3,"                                                                              ");
addLine( 4,"                                                                              ");
addLine( 5,"                                                                              ");
addLine( 6,"                                                                              ");
addLine( 7,"                                                                              ");
addLine( 8,"                                                                              ");
addLine( 9,"                                                                              ");
addLine(10,"                                                                              ");
addLine(11,"                                                                              ");
addLine(12,"                                                                              ");
addLine(13,"                                                                              ");
addLine(14,"                                                                              ");
addLine(15,"                                                                              ");
addLine(16,"                                                                              ");
addLine(17,"                                                                              ");
addLine(18,"                                                                              ");
addLine(19,"                                                                              ");
addLine(20,"                                                                              ");
addLine(21,"                                                                              ");
addLine(22,"                                                                              ");
addLine(23,"                                                                              ");
addLine(24,"                                                                              ");
addLine(25,"                                                                              ");
addLine(26,"                                                                              ");
*/
