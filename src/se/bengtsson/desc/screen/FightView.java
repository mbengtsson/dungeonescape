package se.bengtsson.desc.screen;

import se.bengtsson.desc.creatures.Creature;

/**
 * Fight view class, handles the fight view
 * 
 * @author Marcus Bengtsson
 * 
 */
public class FightView extends Screen {

	private static final long serialVersionUID = -3324073856410019677L;
	private final String TITLE = "DUNGEON ESCAPE - FIGHT";

	/**
	 * Sets up a new fight view
	 * 
	 * @param attacker
	 *            the attacker
	 * @param defender
	 *            the defender
	 */
	public FightView(Creature attacker, Creature defender) {

		addTitle(TITLE);
		addString(37, 7, "\\   /");
		addString(37, 8, " \\ / ");
		addString(37, 9, "  X  ");
		addString(37, 10, "\\/ \\/");
		addString(37, 11, "/\\ /\\");

		addCombatants(attacker, defender);

		addArea(0, 27, 79, 3, ' ');
		addString(31, 27, "[COMBAT RESULTS]");
	}

	/**
	 * Adds creatures stats on the screen
	 * 
	 * @param x
	 *            x-coordinate for top left corner
	 * @param y
	 *            y-coordinate for top left corner
	 * @param creature
	 *            this creatures stats
	 */
	private void addStats(int x, int y, Creature creature) {

		addString(x, y, "<" + creature.getName().toUpperCase() + ">");
		addString(x, y + 2, "Lvl: " + creature.getLevel());
		addString(x, y + 3, "XP:  " + creature.getXp());
		addString(x, y + 5, "HP:  " + creature.getCurrentHp() + " (" + creature.getHp() + "+" + creature.getArmour().getItemStat() + ")");
		addString(x, y + 6, "Pwr: " + creature.getCurrentPwr() + " (" + creature.getPwr() + "+" + creature.getWeapon().getItemStat() + ")");
		addString(x, y + 8, "Armour:");
		addString(x, y + 9, creature.getArmour().getName() + " +" + creature.getArmour().getItemStat() + " HP");
		addString(x, y + 10, "Weapon:");
		addString(x, y + 11, creature.getWeapon().getName() + " +" + creature.getWeapon().getItemStat() + " dmg");

	}

	/**
	 * Adds attacker and defender to the fight view
	 * 
	 * @param attacker
	 *            the attacker
	 * @param defender
	 *            the defender
	 */
	public void addCombatants(Creature attacker, Creature defender) {
		addArea(1, 1, 35, 20, ' ');
		addString(13, 1, "[ATTACKER]");
		addStats(5, 3, attacker);

		addArea(43, 1, 35, 20, ' ');
		addString(55, 1, "[DEFENDER]");
		addStats(47, 3, defender);
	}

	/**
	 * Adds a message to the output area
	 * 
	 * @param output
	 *            a string with the message
	 */
	public void message(String output) {
		addString(1, 28, "                                              ");
		addString(1, 28, ">" + output);
	}

}
