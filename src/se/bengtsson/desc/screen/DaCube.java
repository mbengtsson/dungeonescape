package se.bengtsson.desc.screen;

/**
 * Creates a cube and spins it, because....  well, why not :) 
 * 
 * @author Marcus Bengtsson
 * 
 */
public class DaCube extends Screen {

	private static final long serialVersionUID = -5955058919229217403L;
	private int size;
	private char[][][] cube;
	private char[][][] space;

	/**
	 * Creates a window, a cube and a 3d-space
	 */
	public DaCube() {
		super(79, 35);

		addTitle("DaCube");

		space = new char[35][35][35];
		size = 20;
		cube = cube(size);

	}

	/**
	 * Spins the cube in the 3d-space and projects it on the screen
	 * 
	 * @param time
	 *            duration of the animation
	 */
	public void makeItSpin(int time) {

		time *= 20;

		double angleZ = 0;
		double angleY = 0;
		double angleX = 0;

		while (time > 0) {

			clearSpace();

			cubeToSpace(angleX, angleY, angleZ);

			spaceToScreen();

			addString(1, 33, "Time left: " + time / 20);

			printScr();

			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				System.out.println("Interrupted");
			}

			angleX += 5;
			angleY += 4;
			angleZ += 3;

			fillScr(' ');

			time--;
		}
	}

	/**
	 * projects the 3-d space on the 2-d screen ignoring parallax effects
	 */
	private void spaceToScreen() {

		for (int x = 0; x < 35; x++) {
			for (int y = 0; y < 35; y++) {
				for (int z = 0; z < 35; z++) {

					// don't add empty space
					if (space[z][y][x] != 0) {

						// double width as a character is almost twice as high
						// as wide
						screen[y][7 + x * 2] = space[z][y][x];
						screen[y][7 + x * 2 - 1] = space[z][y][x];
					}
				}
			}
		}
	}

	/**
	 * Adds the cube to the 3-d space at a specified angle (x,y,z)
	 * 
	 * @param angleX
	 *            rotation angle around x-axis in degrees
	 * @param angleY
	 *            rotation angle around y-axis in degrees
	 * @param angleZ
	 *            rotation angle around z-axis in degrees
	 */
	private void cubeToSpace(double angleX, double angleY, double angleZ) {

		// converting angles to radians
		angleZ *= (Math.PI / 180);
		angleY *= (Math.PI / 180);
		angleX *= (Math.PI / 180);

		for (int x = 0; x < size; x++) {
			for (int y = 0; y < size; y++) {
				for (int z = 0; z < size; z++) {

					// just give me the edges
					if (cube[z][y][x] != 0) {

						// coordinates relative to the centre of the cube
						// because the cube should rotate around its centre
						double xr = x - size / 2;
						double yr = y - size / 2;
						double zr = z - size / 2;

						// rotation around z-axis
						double zx = xr * Math.cos(angleZ) - yr * Math.sin(angleZ) - xr;
						double zy = xr * Math.sin(angleZ) + yr * Math.cos(angleZ) - yr;

						// rotation around y-axis
						double yx = (xr + zx) * Math.cos(angleY) - zr * Math.sin(angleY) - (xr + zx);
						double yz = (xr + zx) * Math.sin(angleY) + zr * Math.cos(angleY) - zr;

						// rotation around x-axis
						double xy = (yr + zy) * Math.cos(angleX) - (zr + yz) * Math.sin(angleX) - (yr + zy);
						double xz = (yr + zy) * Math.sin(angleX) + (zr + yz) * Math.cos(angleX) - (zr + yz);

						// offset for the new coordinates (-0.5 for proper
						// rounding)
						int xOffset = (int) (yx + zx - 0.5);
						int yOffset = (int) (zy + xy - 0.5);
						int zOffset = (int) (xz + yz - 0.5);

						// places character in space at the new coordinates
						// based on rotation
						space[7 + z + zOffset][7 + y + yOffset][7 + x + xOffset] = cube[z][y][x];
					}
				}
			}
		}
	}

	/**
	 * Clears the space, fills it with null
	 */
	private void clearSpace() {
		for (int x = 0; x < 35; x++) {
			for (int y = 0; y < 35; y++) {
				for (int z = 0; z < 35; z++) {

					space[z][y][x] = 0;

				}
			}
		}
	}

	/**
	 * Generates a cube as a 3d-char-array 
	 * 
	 * @param size
	 *            the cubes size
	 * @return the cube as a 3 dimensional array
	 */
	private char[][][] cube(int size) {

		char[][][] cube = new char[size][size][size];

		for (int z = 0; z < size; z++) {
			for(int y = 0; y < size; y++){
				for(int x = 0; x < size; x++){

					if((x == 0 && z == 0) || (x == size - 1 && z == 0) || (y == 0 && z == 0) || (y == size - 1 && z == 0) || 
							(x == 0 && z == size - 1) || (x == size - 1 && z == size - 1) || (y == 0 && z == size - 1) || (y == size - 1 && z == size - 1) || 
							(x == 0 && y == 0) || (x == size - 1 && y == 0) || (x == 0 && y == size - 1) || (x == size - 1 && y == size - 1)) {
						//Edges
						cube[z][y][x] = '#';
					}
					else {
						//Fill
						cube[z][y][x] = 0;
					}
				}
			}
		}

		return cube;
	}
}
