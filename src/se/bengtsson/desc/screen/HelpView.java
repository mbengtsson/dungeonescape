package se.bengtsson.desc.screen;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Help view class, handles the help view
 * 
 * @author Marcus Bengtsson
 * 
 */
public class HelpView extends Screen {

	private static final long serialVersionUID = 3890509992965485588L;

	/**
	 * Sets up the help view
	 */
	public HelpView() {

		addTitle("DUNGEON ESCAPE - HELP");

		addString(5, 2, "      Dungeon Escape - a rougelike by Marcus Bengtsson GBJU13");
		addString(5, 3, "      -------------------------------------------------------");

		addString(5, 5, "COMMANDS");
		addString(5, 7, "Command         Description");
		addString(5, 8, "wait            Wait for one turn");
		addString(5, 9, "n/north         Move north");
		addString(5, 10, "s/south         Move south");
		addString(5, 11, "e/east          Move east");
		addString(5, 12, "w/west          Move west");
		addString(5, 13, "inv/inventory   Show your inventory");
		addString(5, 14, "load            Load or delete a saved game");
		addString(5, 15, "save            Save the game");
		addString(5, 16, "help            Show this screen");
		addString(5, 17, "exit            Exit the game");

		addString(5, 19, "spin            A spinning cube in the console :)");

		addString(45, 5, "SYMBOLS");
		addString(45, 7, "@ - The player");
		addString(45, 8, "B - Boss");
		addString(45, 9, "M - Monster");
		addString(45, 10, "E - The exit");
		addString(45, 11, "# - Wall");
		addString(45, 12, ": - Unexplored area");
		
		addString(5, 21, "* Attack enemies by moving on them");
		addString(5, 22, "* The exit opens when all enemies in the area are dead");
		addString(5, 23, "  and let you move to the next area");
		addString(5, 24, "* When defeating an oponent you will automaticly loot their");
		addString(5, 25, "  weapon and armour if you don't already have the same piece");
		addString(5, 26, "  in your inventory");

		addString(5, 28, "Press enter to return...");
	}

	/**
	 * Shows the help view
	 */
	public void showHelp() {
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		printScr();

		try {
			input.readLine();
		} catch (IOException e) {
			System.out.println("Something went wrong");
		}
	}

}
