package se.bengtsson.desc.screen;

import se.bengtsson.desc.creatures.Player;

/**
 * Main view class, handles the main game view
 * 
 * @author Marcus Bengtsson
 * 
 */
public class MainView extends Screen {

	private static final long serialVersionUID = 1102610889068118640L;
	private final String TITLE = "DUNGEON ESCAPE";

	/**
	 * Creates a main-view screen
	 * 
	 * @param player
	 *            the player
	 */
	public MainView(Player player) {

		addTitle(TITLE);

		addArea(0, 1, 60, 29, '#');
		addString(28, 1, "[MAP]");
		addString(60, 20, " Type \"help\"");
		addString(60, 21, " for help");
		addString(60, 27, "-----[OUTPUT]-----");
		addStats(player);

	}

	/**
	 * Adds the map to the main-view screen
	 * 
	 * @param map
	 *            the map
	 */
	public void setMap(char[][] map) {

		int height = map.length;
		int width = map[0].length;

		for (int y = 2; y < height + 2; y++) {
			for (int x = 1; x < width + 1; x++) {
				screen[y][x] = map[y - 2][x - 1];
			}
		}
	}

	/**
	 * adds players stats to the main-view screen
	 * 
	 * @param player
	 *            the player
	 */
	public void addStats(Player player) {

		addString(61, 3, "<" + player.getName().toUpperCase() + ">");
		addString(61, 5, "Lvl: " + player.getLevel());
		addString(61, 6, "XP:  " + player.getXp() + " (" + player.getLevel() * player.getLevel() * 5 + ")");
		addString(61, 8, "                 ");
		addString(61, 8, "HP:  " + player.getCurrentHp() + " (" + player.getHp() + "+" + player.getArmour().getItemStat() + ")");
		addString(61, 9, "                 ");
		addString(61, 9, "Pwr: " + player.getCurrentPwr() + " (" + player.getPwr() + "+" + player.getWeapon().getItemStat() + ")");
		addString(61, 11, "Armour:");
		addString(61, 12, "                 ");
		addString(61, 12, player.getArmour().getName() + " +" + player.getArmour().getItemStat() + " HP");
		addString(61, 13, "Weapon:");
		addString(61, 14, "                 ");
		addString(61, 14, player.getWeapon().getName() + " +" + player.getWeapon().getItemStat() + " dmg");

	}

	/**
	 * Adds a message to the message area to
	 * the main-view screen
	 * 
	 * @param output
	 *            a string with the message
	 */
	public void message(String output) {
		addString(60, 28, "                  ");
		addString(60, 28, ">" + output);
	}

}
