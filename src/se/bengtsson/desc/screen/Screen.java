package se.bengtsson.desc.screen;

import java.io.Serializable;

import org.fusesource.jansi.AnsiConsole;

/**
 * Screen class, this class is used to create and handle a "window" in the
 * console all views inherits this class
 * 
 * @author Marcus Bengtsson
 * 
 */
public abstract class Screen implements Serializable {

	private static final long serialVersionUID = -8118545805219406733L;
	public final int WIDTH;
	public final int HEIGHT;

	protected char[][] screen;

	/**
	 * Creates an empty screen with size 79*30
	 */
	public Screen() {
		this(79, 30);
	}

	/**
	 * Creates an empty screen with a custom size
	 * 
	 * @param width
	 *            screen width
	 * @param height
	 *            screen height
	 */
	public Screen(int width, int height) {
		this(' ', width, height);
	}

	/**
	 * creates a scrren with a custom size an fill it with a character
	 * 
	 * @param f
	 *            Character to fill screen with
	 * @param width
	 *            screen width
	 * @param height
	 *            screen height
	 */
	public Screen(char f, int width, int height) {

		WIDTH = width;
		HEIGHT = height;

		screen = new char[HEIGHT][WIDTH];

		for (int y = 0; y < HEIGHT; y++) {
			for (int x = 0; x < WIDTH; x++) {

				if ((x == 0 && y == 0) || (x == WIDTH - 1 && y == 0) || (x == 0 && y == HEIGHT - 1) || (x == WIDTH - 1 && y == HEIGHT - 1)) {
					screen[y][x] = '+';
				} else if (x == 0 || x == WIDTH - 1) {
					screen[y][x] = '|';
				} else if (y == 0 || y == HEIGHT - 1) {
					screen[y][x] = '-';
				} else {
					screen[y][x] = f;
				}

			}
		}
	}

	/**
	 * Adds a title to the screen
	 * 
	 * @param title
	 *            the title
	 */
	public void addTitle(String title) {

		title = "[" + title + "]";
		int pos = (WIDTH - title.length()) / 2;

		addString(pos, 0, title);

	}

	/**
	 * Fills the screen with a character
	 * 
	 * @param f
	 *            character to fill the screen with
	 */
	public void fillScr(char f) {

		for (int y = 1; y < HEIGHT - 1; y++) {
			for (int x = 1; x < WIDTH - 1; x++) {
				screen[y][x] = f;
			}
		}
	}

	/**
	 * Creates an area with a border on the screen
	 * 
	 * @param xPos
	 *            top left x position
	 * @param yPos
	 *            top left y position
	 * @param height
	 *            areas height
	 * @param width
	 *            areas width
	 * @param f
	 *            character to fill area
	 * @return true if it fits on the screen false if it is to big
	 */
	public boolean addArea(int xPos, int yPos, int width, int height, char f) {

		width += xPos;
		height += yPos;

		if (width > WIDTH || height > HEIGHT) {
			return false;
		}

		for (int y = yPos; y < height; y++) {
			for (int x = xPos; x < width; x++) {

				if ((x == xPos && y == yPos) || (x == width - 1 && y == yPos) || (x == xPos && y == height - 1) || (x == width - 1 && y == height - 1)) {
					screen[y][x] = '+';
				} else if (x == xPos || x == width - 1) {
					screen[y][x] = '|';
				} else if (y == yPos || y == height - 1) {
					screen[y][x] = '-';
				} else {
					screen[y][x] = f;
				}
			}
		}

		return true;
	}

	/**
	 * Adds a string to the screen
	 * 
	 * @param xPos
	 *            x-coordinate
	 * @param yPos
	 *            y-coordinate
	 * @param s
	 *            the string
	 * @return true if it fits on the screen false if it is to big
	 */
	public boolean addString(int xPos, int yPos, String s) {

		int width = xPos + s.length();
		int i = 0;

		if (width > WIDTH) {
			return false;
		}

		for (int x = xPos; x < width; x++) {

			screen[yPos][x] = s.charAt(i);
			i++;
		}
		return true;

	}

	/**
	 * Prints the screen to the console
	 */
	public void printScr() {

		clrScr();

		for (int y = 0; y < HEIGHT; y++) {
			for (int x = 0; x < WIDTH; x++) {
				System.out.print(screen[y][x]);
			}
			System.out.println();
		}
	}

	/**
	 * clear the console, uses jansi to make the console accept ANSI escape
	 * sequences http://jansi.fusesource.org/
	 * 
	 * \u001b = escape, [2J = clear screen, [H = home cursor
	 */
	public void clrScr() {

		AnsiConsole.out.println("\u001b[2J \u001b[H");

	}
}
