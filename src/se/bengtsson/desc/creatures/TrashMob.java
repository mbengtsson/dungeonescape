package se.bengtsson.desc.creatures;

/**
 * Trash mob class, creates trash mobs
 * 
 * @author Marcus Bengtsson
 * 
 */
public class TrashMob extends Npc {

	private static final long serialVersionUID = 1910895939352046286L;

	/**
	 * Constructor, creates a new trash npc
	 * 
	 * @param level
	 *            initial level
	 * @param name
	 *            npc name
	 */
	public TrashMob(int level, String name) {

		super(level, name, 'M', Stats.RANDOM_MOB.getInvSize(), true);

		hp = rndStat(Stats.RANDOM_MOB.getHpMin(), Stats.RANDOM_MOB.getHpMax()) + (level - 1) * 3;
		totalHp = hp;
		currentHp = totalHp;

		pwr = rndStat(Stats.RANDOM_MOB.getPwrMin(), Stats.RANDOM_MOB.getPwrMax()) + (level - 1) * 3;
		totalPwr = pwr;
		currentPwr = totalPwr;

	}
}
