package se.bengtsson.desc.creatures;

import java.io.Serializable;
import java.util.Random;

import se.bengtsson.desc.dungeon.Maps;
import se.bengtsson.desc.items.Empty;
import se.bengtsson.desc.items.Item;

/**
 * Abstract creature class, all creatures inherits this class
 * 
 * @author Marcus Bengtsson
 * 
 */
public abstract class Creature implements Serializable {

	private static final long serialVersionUID = 3125361041313481841L;

	public final char SYMBOL;

	private boolean alive = true;

	private int xPos;
	private int yPos;

	private String name;
	protected int level;
	private int xp;

	protected int hp;
	protected int pwr;
	protected int currentHp;
	protected int currentPwr;
	protected int totalHp;
	protected int totalPwr;

	private Inventory inventory;

	private Item armour;
	private Item weapon;

	/**
	 * Constructor, creats a new creature
	 * 
	 * @param level
	 *            initial level
	 * @param name
	 *            creature name
	 * @param sign
	 *            sign to represent the creature
	 * @param invSize
	 *            size of creatures inventory
	 */
	public Creature(int level, String name, char sign, int invSize) {
		SYMBOL = sign;
		this.name = name;
		this.level = level;
		inventory = new Inventory(invSize);

		weapon = new Empty();
		armour = new Empty();

	}

	/**
	 * Randomize an int in an interval
	 * 
	 * @param min
	 *            low limit of the interval
	 * @param max
	 *            high limit of the interval
	 * @return a random integer in the interval
	 */
	protected int rndStat(int min, int max) {

		if (max == min) {
			return max;
		}

		Random rnd = new Random();
		return rnd.nextInt(max - min) + min;
	}

	/**
	 * get creatures name
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * get creatures level
	 * 
	 * @return level
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * get creatures experience
	 * 
	 * @return xp
	 */
	public int getXp() {
		return xp;
	}

	/**
	 * get creatures inventory
	 * 
	 * @return inventory
	 */
	public Inventory getInv() {
		return inventory;
	}

	/**
	 * get creatures slotted weapon
	 * 
	 * @return weapon item
	 */
	public Item getWeapon() {
		return weapon;
	}

	/**
	 * get creatures slotted armour
	 * 
	 * @return armour item
	 */
	public Item getArmour() {
		return armour;
	}

	/**
	 * Get creatures current health
	 * 
	 * @return current hp
	 */
	public int getCurrentHp() {
		return currentHp;
	}

	/**
	 * get creatures base health
	 * 
	 * @return base hp
	 */
	public int getHp() {
		return hp;
	}

	/**
	 * get creatures total health after modifiers
	 * 
	 * @return total hp
	 */
	public int getTotalHp() {
		return totalHp;
	}

	/**
	 * Get creatures current power
	 * 
	 * @return current pwr
	 */
	public int getCurrentPwr() {
		return currentPwr;
	}

	/**
	 * get creatures base power
	 * 
	 * @return base pwr
	 */
	public int getPwr() {
		return pwr;
	}

	/**
	 * get creatures total power after modifiers
	 * 
	 * @return total pwr
	 */
	public int getTotalPwr() {
		return totalPwr;
	}

	/**
	 * get creatures x coordinate
	 * 
	 * @return x coordinate
	 */
	public int getX() {
		return xPos;
	}

	/**
	 * get creatures y coordinate
	 * 
	 * @return y coordinate
	 */
	public int getY() {
		return yPos;
	}

	/**
	 * Equips a weapon
	 * 
	 * @param weapon
	 *            weapon to equip
	 * @return true if success false if fail (ie. not a weapon)
	 */
	public boolean setWeapon(Item weapon) {
		if (weapon.getType() == 1) {
			this.weapon = weapon;
			totalPwr = pwr + weapon.getItemStat();
			currentPwr = totalPwr;
			return true;
		}
		return false;
	}

	/**
	 * Equips an armour
	 * 
	 * @param armour
	 *            armour to equip
	 * @return true if success false if fail (ie. not an armour)
	 */
	public boolean setArmour(Item armour) {
		if (armour.getType() == 2) {
			this.armour = armour;
			totalHp = hp + armour.getItemStat();
			currentHp = totalHp;
			return true;
		}
		return false;
	}

	/**
	 * set creatures location
	 * 
	 * @param x
	 *            x coordinate
	 * @param y
	 *            y coordinate
	 */
	public void setLocation(int x, int y) {
		xPos = x;
		yPos = y;
	}

	/**
	 * set current health
	 * 
	 * @param currentHp
	 *            current hp
	 */
	public void setCurrentHp(int currentHp) {
		this.currentHp = currentHp;
	}

	/**
	 * set current power
	 * 
	 * @param currentPwr
	 *            current pwr
	 */
	public void setCurrentPwr(int currentPwr) {
		this.currentPwr = currentPwr;
	}

	/**
	 * Level up creature
	 * 
	 * @return true if success, false if fail
	 */
	public boolean levelUp() {

		level++;
		hp += 3;
		pwr += 3;

		totalHp = hp + getArmour().getItemStat();
		totalPwr = pwr + getWeapon().getItemStat();
		currentHp = totalHp;
		currentPwr = totalPwr;

		return true;
	}

	/**
	 * Increases xp after defeatin an other creature
	 * 
	 * @param target
	 *            defeated creature
	 */
	public void addXP(Creature target) {

		this.xp += target.getLevel() + (target.getPwr() / 5);

	}

	/**
	 * get defeated creatures items
	 * 
	 * @param target
	 *            creature to loot
	 */
	public void loot(Creature target) {

		if (!target.getArmour().getName().equals("Empty")) {
			getInv().addItem(target.getArmour());
		}
		if (!target.getWeapon().getName().equals("Empty")) {
			getInv().addItem(target.getWeapon());
		}
	}
	
	/**
	 * Places the npc randomly on the map
	 * 
	 * @param map
	 *            to place npc on
	 */
	public void setRandomLocation(Maps map) {

		Random rnd = new Random();

		int x = rnd.nextInt(58);
		int y = rnd.nextInt(26);

		if (map.getMap()[y][x] == ' ') {
			setLocation(x, y);
		} else {
			setRandomLocation(map);
		}
	}

	/**
	 * Moves the creature on the map
	 * 
	 * @param map
	 *            map to move on
	 * @param direction
	 *            string with the direction to move (north, south, east or west)
	 * @return collision type if collision detected (0-empty, 1-wall, 2-npc,
	 *         3-player, 4-exit)
	 */
	public int move(Maps map, String direction) {

		int newX;
		int newY;

		switch (direction) {
		case "north":
			newX = xPos;
			newY = yPos - 1;
			break;
		case "south":
			newX = xPos;
			newY = yPos + 1;
			break;
		case "west":
			newX = xPos - 1;
			newY = yPos;
			break;
		case "east":
			newX = xPos + 1;
			newY = yPos;
			break;
		default:
			newX = xPos;
			newY = yPos;
			break;
		}

		int type = collisionCheck(map, newX, newY);

		if (type == 0 || type == 2) {
			xPos = newX;
			yPos = newY;
		}
		return type;

	}

	/**
	 * Sets the alive flag to false
	 */
	public void kill() {
		alive = false;
	}

	/**
	 * Checks if creature is alive
	 * 
	 * @return true if alive, false if dead
	 */
	public boolean isAlive() {
		return alive;
	}

	/**
	 * Checks for collision when moving creature
	 * 
	 * @param map
	 *            map to check for collisions
	 * @param x
	 *            x-coordinate to check
	 * @param y
	 *            y-coordinate to check
	 * @return collision type (0-empty, 1-wall, 2-npc, 3-player, 4-exit)
	 */
	public int collisionCheck(Maps map, int x, int y) {

		char tile = map.getMap()[y][x];

		if (tile == 'B' || tile == 'M') {
			tile = 'M';
		}

		switch (tile) {
		case ' ':
			return 0;
		case '#':
			return 1;
		case 'M':
			return 2;
		case '@':
			return 3;
		case 'E':
			return 4;
		default:
			return -1;
		}
	}
}
