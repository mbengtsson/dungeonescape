package se.bengtsson.desc.creatures;

/**
 * This enumeration contains stats for different creatures
 * 
 * @author Marcus Bengtsson
 * 
 */
public enum Stats {

	PLAYER(20, 25, 10, 12, 20), 
	RANDOM_MOB(15, 20, 7, 12, 0), 
	BOSS_MOB(25, 35, 12, 16, 0);

	private int hpMin;
	private int hpMax;
	private int pwrMin;
	private int pwrMax;
	private int invSize;

	/**
	 * 
	 * @param hpMin
	 *            min health
	 * @param hpMax
	 *            max health
	 * @param pwrMin
	 *            min power
	 * @param pwrMax
	 *            max power
	 * @param invSize
	 *            inventory size
	 */
	private Stats(int hpMin, int hpMax, int pwrMin, int pwrMax, int invSize) {

		this.hpMin = hpMin;
		this.hpMax = hpMax;
		this.pwrMin = pwrMin;
		this.pwrMax = pwrMax;
		this.invSize = invSize;
	}

	/**
	 * get min health
	 * 
	 * @return min health
	 */
	public int getHpMin() {
		return hpMin;
	}

	/**
	 * get max health
	 * 
	 * @return max health
	 */
	public int getHpMax() {
		return hpMax;
	}

	/**
	 * get min power
	 * 
	 * @return min power
	 */
	public int getPwrMin() {
		return pwrMin;
	}

	/**
	 * get max power
	 * 
	 * @return max power
	 */
	public int getPwrMax() {
		return pwrMax;
	}

	/**
	 * get inventory size
	 * 
	 * @return inventory size
	 */
	public int getInvSize() {
		return invSize;
	}
}
