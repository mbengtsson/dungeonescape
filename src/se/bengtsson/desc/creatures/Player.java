package se.bengtsson.desc.creatures;

/**
 * Player class, creates a player
 * 
 * @author Marcus Bengtsson
 * 
 */
public class Player extends Creature {

	private static final long serialVersionUID = 1574904679829933867L;

	/**
	 * Constructor, creates a new player
	 * 
	 * @param level
	 *            players initial level
	 * @param name
	 *            players name
	 * @param sign
	 *            sign to represent player on the map
	 */
	public Player(int level, String name, char sign) {

		super(level, name, sign, Stats.PLAYER.getInvSize());

		hp = rndStat(Stats.PLAYER.getHpMin(), Stats.PLAYER.getHpMax());
		totalHp = hp;
		currentHp = totalHp;

		pwr = rndStat(Stats.PLAYER.getPwrMin(), Stats.PLAYER.getPwrMax());
		totalPwr = pwr;
		currentPwr = totalPwr;

	}

	/**
	 * Level up player if experience is greater or equal to level² * 5
	 * 
	 * @return true if success, false if fail
	 */
	@Override
	public boolean levelUp() {

		if (getXp() >= getLevel() * getLevel() * 5) {

			level++;
			hp += 3;
			pwr += 3;

			totalHp = hp + getArmour().getItemStat();
			totalPwr = pwr + getWeapon().getItemStat();
			currentHp = totalHp;
			currentPwr = totalPwr;

			return true;
		}

		return false;
	}

}
