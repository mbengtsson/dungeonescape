package se.bengtsson.desc.creatures;

import se.bengtsson.desc.dungeon.Maps;

/**
 * Ai interface, classes implementing it needs a method to control
 * automatic movement and behaviour of the creature
 * 
 * @author Marcus Bengtsson
 * 
 */
public interface Ai {

	/**
	 * If an aggressive npc detects the character it moves toward him else the npc moves
	 * randomly.
	 * 
	 * @param map
	 *            map to detect collisions on
	 * @param player
	 *            player to detect
	 * @return true if npc moves to the same tile as the player
	 */
	public boolean moveNpc(Maps map, Player player);

}
