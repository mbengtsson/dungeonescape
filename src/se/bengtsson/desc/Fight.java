package se.bengtsson.desc;

import java.util.Random;

import se.bengtsson.desc.creatures.Creature;
import se.bengtsson.desc.screen.FightView;

/**
 * Fight class that handles fights between creatures
 * 
 * @author Marcus Bengtsson
 * 
 */
public class Fight {

	private Creature attacker;
	private Creature target;
	private FightView view;

	/**
	 * Constructor sets attacker and target and creates a fight view
	 * 
	 * @param attacker
	 *            the attacker
	 * @param target
	 *            the target
	 */
	public Fight(Creature attacker, Creature target) {

		this.attacker = attacker;
		this.target = target;

		view = new FightView(this.attacker, this.target);
	}

	/**
	 * Starts a fight between the attacker and the target
	 * 
	 * @return returns true if attacker wins and false if target wins
	 */
	public boolean startFight() {

		Random rnd = new Random();
		int damage;

		view.message(attacker.getName() + " starts");

		while (attacker.getCurrentHp() > 0) {

			view.printScr();

			try {
				Thread.sleep(1500);
			} catch (InterruptedException e) {

			}

			damage = rnd.nextInt(attacker.getPwr());
			target.setCurrentHp(target.getCurrentHp() - damage);

			// Fix to not show negative HP
			if (target.getCurrentHp() < 0) {
				target.setCurrentHp(0);
			}

			view.addCombatants(attacker, target);
			view.message(attacker.getName() + " hits " + target.getName() + " for " + damage + " damage!");
			view.printScr();

			try {
				Thread.sleep(1200);
			} catch (InterruptedException e) {
				System.out.println("Interrupted");
			}

			if (target.getCurrentHp() < 1) {

				attacker.addXP(target);
				target.kill();
				return true;
			}

			damage = rnd.nextInt(target.getPwr());
			attacker.setCurrentHp(attacker.getCurrentHp() - damage);

			// Fix to not show negative HP
			if (attacker.getCurrentHp() < 0) {
				attacker.setCurrentHp(0);
			}

			view.addCombatants(attacker, target);
			view.message(target.getName() + " hits " + attacker.getName() + " for " + damage + " damage!");

		}

		view.printScr();

		try {
			Thread.sleep(1200);
		} catch (InterruptedException e) {
			System.out.println("Interrupted");
		}

		target.addXP(attacker);
		attacker.kill();
		return false;

	}

}
