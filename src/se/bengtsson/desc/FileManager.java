package se.bengtsson.desc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;

import se.bengtsson.desc.creatures.Player;

/**
 * File manager class that handles loading and saving games
 * 
 * @author Marcus Bengtsson
 * 
 */
public class FileManager {

	/**
	 * Saves the game and adds a reference to it in the saves.txt file
	 * 
	 * @param game
	 *            game to save
	 * @param player
	 *            current player
	 */
	public void saveGame(Game game, Player player) {

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		ArrayList<String> saves;
		saves = getSaves();
		

		if (saves == null) {
			System.out.println("No previous saves\r\n");
		
			saves = new ArrayList<String>();
		}
		
		if(game.isClassic()) {
			
			int counter = 0;
			
			for (int i = 0; i < saves.size(); i++) {
				String[] element = saves.get(i).split(";");
				if (element[0].equals("+" + player.getName() + counter)) {
					counter++;
				}
			}
			
			String name = "+" + player.getName() + counter;
			
			saves.add(String.format("%s;%s;%s;%s", name, player.getName(), player.getLevel(), Calendar.getInstance().getTime().toString()));
			writeSaves(saves);
			save(game, name);
			
			return;
		}

		if (!saves.isEmpty()) {
			System.out.println("Previously saved games:\r\n");
			System.out.println("Save\t\tPlayer\tLevel\tDate");

			for (String line : saves) {

				String[] element = line.split(";");
				System.out.println(String.format("%s\t\t%s\t%s\t%s", element[0], element[1], element[2], element[3]));

			}

			System.out.println("\r\nEnter a name for your save, you can overwrite an old save by using the same name\r\njust press enter to cancel");
			System.out.print("Save as> ");
			System.out.flush();

			try {
				String name = input.readLine();

				if (name.equals("")) {
					return;
				}

				for (int i = 0; i < saves.size(); i++) {
					String[] element = saves.get(i).split(";");
					if (element[0].equals(name)) {
						saves.remove(i);
						break;
					}
				}

				saves.add(String.format("%s;%s;%s;%s", name, player.getName(), player.getLevel(), Calendar.getInstance().getTime().toString()));

				writeSaves(saves);
				save(game, name);

			} catch (IOException e) {
				System.out.println("Couldn't read that.");
			}

		} else {
			System.out.println("\r\nEnter a name for your save\r\njust press enter to cancel");
			System.out.print("Save as> ");
			System.out.flush();

			try {
				String name = input.readLine();
				if (name.equals("")) {
					return;
				}

				saves.add(String.format("%s;%s;%s;%s", name, player.getName(), player.getLevel(), Calendar.getInstance().getTime().toString()));

				writeSaves(saves);
				save(game, name);

			} catch (IOException e) {
				System.out.println("Couldn't read that.");
			}
		}
	}

	/**
	 * Loads or deletes a saved game, if trying to load a game that does not exist
	 * it will remove it's reference in the saved.txt file
	 * 
	 * @return loaded game
	 */
	public Game loadGame() {
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		ArrayList<String> saves;
		saves = getSaves();

		if (saves == null || saves.size() == 0) {
			System.out.println("No previous saves.\r\n");

			return null;
		}

		System.out.println("Previously saved games:\r\n");
		System.out.println("Nr. Save\t\tPlayer\tLevel\tDate");

		for (int i = 0; i < saves.size(); i++) {

			String[] element = saves.get(i).split(";");
			System.out.println(String.format("% 2d. %s\t\t%s\t%s\t%s", i + 1, element[0], element[1], element[2], element[3]));

		}
		System.out.println("\r\nEnter number of game to load\r\nsaves with prefix '+' are saved in classic mode and deleted when loaded\r\ntype delete <save number> to delete a save\r\njust press enter to cancel\r\n");
		System.out.print("load> ");
		System.out.flush();

		try {
			String usrIn = input.readLine();

			if (usrIn.equals("")) {
				return null;
			}

			String[] commands = usrIn.split(" ");

			int index = -1;

			if (commands.length == 1) {

				try {
					index = Integer.parseInt(usrIn) - 1;
				} 
				catch (NumberFormatException e) {
					System.out.println("Invalid input");

				}


				if (index >= 0 && index < saves.size()) {
					String[] save = saves.get(index).split(";");

					Game game = load(save[0]);

					//System.out.println(game);

					if (game == null) {

						saves.remove(index);
						writeSaves(saves);
						System.out.println("That save file does not exist, removing entry.\r\n");

					}

					if(game != null && game.isClassic()) {
						
						delete(save[0]);
						
						saves.remove(index);
						writeSaves(saves);
					}
					
					return game;
				} else {

					System.out.println("No such save.\r\n");
					return null;

				}
			} else if (commands[0].toLowerCase().equals("delete") || commands[0].toLowerCase().equals("del")) {

				try {
					index = Integer.parseInt(commands[1]) - 1;
				}
				catch (NumberFormatException e){
					System.out.println("Invalid input");
				}

				if (index >= 0 && index < saves.size()) {
					String[] save = saves.get(index).split(";");

					delete(save[0]);

					saves.remove(index);
					writeSaves(saves);
					
					System.out.println(save[0] + " deleted");
				}


			}

			return null;

		} catch (IOException e) {
			System.out.println("Couldn't read that.\r\n");

			return null;
		}

	}

	/**
	 * Saves the game to a file, adds *.sav to the filename
	 * 
	 * @param game
	 *            Game to save
	 * @param file
	 *            File name, adds *.sav
	 * @return true if success, false if fail
	 */
	private boolean save(Game game, String file) {

		file = file + ".sav";

		ObjectOutputStream save = null;
		try {
			save = new ObjectOutputStream(new FileOutputStream(file));
			save.writeObject(game);

		} catch (IOException e) {
			return false;
		} finally {
			try {
				save.close();
			} catch (IOException e) {
				System.out.println("couldn't close file");
			}
		}

		return true;
	}

	/**
	 * Loads a saved game if it exists, adds *.sav to the filename
	 * 
	 * @param file
	 *            File name, adds *.sav
	 * @return the loaded game, if load fails it returns null
	 */
	private Game load(String file) {

		file = file + ".sav";

		ObjectInputStream restore = null;
		Object obj = null;

		File saveFile = new File(file);

		if (saveFile.exists()) {

			try {
				restore = new ObjectInputStream(new FileInputStream(saveFile));
				obj = restore.readObject();

			} catch (IOException e) {
				return null;
			} catch (ClassNotFoundException e) {
				return null;
			} finally {
				try {
					restore.close();
				} catch (IOException e) {
					System.out.println("couldn't close file");
				} catch (NullPointerException e) {
					return null;
				}
			}

			if (obj instanceof Game) {
				return (Game) obj;
			} else {
				return null;
			}

		} else {
			return null;
		}
	}
	
	
	/**
	 * Deletes a save file (*.sav)
	 * 
	 * @param file file to delete
	 * @return true if file existed and is deleted, false if it didn't exist
	 */
	private boolean delete(String file){
		
		file = file + ".sav";
		
		File file2 = new File(file);
		
		if (file2.exists()) {
			
			file2.delete();
			return true;
		}
		
		
		return false;
		
	}

	/**
	 * Loads a list of all save files from saves.txt
	 * 
	 * @return an array list with all save files, return null if there are no
	 *         list saved
	 */
	private ArrayList<String> getSaves() {

		ArrayList<String> saves = new ArrayList<String>();

		BufferedReader reader = null;

		File file = new File("saves.txt");

		if (file.exists()) {
			try {

				reader = new BufferedReader(new FileReader(file));

				while (true) {
					String line = reader.readLine();

					if (line == null) {
						break;
					}
					saves.add(line);
				}
			}

			catch (IOException e) {
				return null;
			} finally {
				try {
					reader.close();
				} catch (IOException e) {
					System.out.println("Couldn't close file");
				}
			}

			return saves;

		} else {
			return null;

		}

	}


	/**
	 * Saves a list of all save files to saves.txt
	 * 
	 * @param saves array-list with all saves
	 * @return true if it success and false if it fails
	 */
	private boolean writeSaves(ArrayList<String> saves) {

		BufferedWriter writer = null;

		try {
			writer = new BufferedWriter(new FileWriter("saves.txt"));

			for (String line : saves) {
				writer.write(line);
				writer.newLine();
			}

		} catch (IOException e) {

			return false;
		} finally {
			try {
				writer.close();
			} catch (IOException e) {
				System.out.println("Couldn't close file");
			}
		}

		return true;
	}
}
