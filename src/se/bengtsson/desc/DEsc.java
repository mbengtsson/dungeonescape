package se.bengtsson.desc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.fusesource.jansi.AnsiConsole;

import se.bengtsson.desc.screen.Animation;

/**
 * Dungeon Escape - a rougelike game (http://en.wikipedia.org/wiki/Roguelike)
 * 
 * exam for the course "grundläggande programering och javaverktyg (basic programming and java tools)" - Teknikhögskolan 2013
 * 
 * by Marcus Bengtsson GBJU13
 * 
 * @author Marcus Bengtsson
 * 
 */
public class DEsc implements Runnable {

	private boolean running = false;
	private Thread thread;
	private Game game;

	/**
	 * Main method - it all starts here
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		DEsc dEsc = new DEsc();
		dEsc.start();
	}

	/**
	 * Run method, setting up a game and run it
	 */
	@Override
	public void run() {

		AnsiConsole.systemInstall();

		String choise = "";

		AnsiConsole.out.println("\u001b[2J \u001b[H");

		Animation welcome = new Animation(2);
		welcome.animateDown(50);

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Do you want to load a saved game (Y/n)> ");
		System.out.flush();

		try {
			choise = input.readLine();
		} catch (IOException e) {
			System.out.println("Something went wrong, couldn't read your command\r\nLoading saved game");
		}

		if (choise.toLowerCase().equals("n") || choise.toLowerCase().equals("no")) {

			game = new Game();
		} else {
			FileManager fm = new FileManager();

			game = fm.loadGame();

			if (game == null) {
				game = new Game();
			}

		}

		while (running) {
			game = game.nextTurn();

			if (game.isHalted()) {
				running = false;
			}
		}

		AnsiConsole.systemUninstall();
	}

	/**
	 * Start method, starts the thread and sets running to true
	 */
	public void start() {
		if (running) {
			return;
		}
		running = true;
		thread = new Thread(this);
		thread.start();
	}

	/**
	 * Stop method, stops the thread and sets running to false
	 */
	public void stop() {
		if (!running) {
			return;
		}
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			System.out.println("Something went wrong");
			System.exit(1);
		}
	}
}