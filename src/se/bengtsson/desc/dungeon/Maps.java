package se.bengtsson.desc.dungeon;

import java.io.Serializable;

import se.bengtsson.desc.creatures.Creature;
import se.bengtsson.desc.creatures.Player;

/**
 * Map class, handles maps in game
 * 
 * @author Marcus Bengtsson
 * 
 */
public class Maps implements Serializable {

	private static final long serialVersionUID = -4159535968881914255L;
	public final int WIDTH;
	public final int HEIGHT;

	private char[][] map;
	private char[][] fow;
	private char[][] visibleMap;
	private int area;

	private DungeonGen generator;

	/**
	 * Creates a map for the chosen area
	 * 
	 * @param area
	 *            area to create map for
	 */
	public Maps(int area) {
		WIDTH = 58;
		HEIGHT = 27;

		map = new char[HEIGHT][WIDTH];
		fow = new char[HEIGHT][WIDTH];
		visibleMap = new char[HEIGHT][WIDTH];
		
		generator = DungeonGen.getInstance();
		
		setArea(area);

	}

	/**
	 * loads a random dungeon to the map if it is a new area,
	 * if it is the current area it loads the current dungeon to the map 
	 * 
	 * @param area
	 *            area to load
	 */
	public void setArea(int area) {
		
		if (this.area != area){
			fow = new char[HEIGHT][WIDTH];
			generator.genDungeon();
		}
		
		this.area = area;
		map = loadDungeon();
	}
	
	/**
	 * Returns a copy of the current dungeon from 
	 * the random dungeon generator
	 * 
	 * @return copy of current dungeon as a 2d char-array
	 */
	private char[][] loadDungeon(){
		char[][] temp = new char[HEIGHT][WIDTH];
		
		for(int y = 0; y < HEIGHT; y++){
			for(int x = 0; x < WIDTH; x++){
				temp[y][x] = generator.getDungeon()[y][x];
			}
		}
		return temp;
	}

	/**
	 * adds a creature to the map, if the creature is the player
	 * fog of war is removed around him
	 * 
	 * @param creature
	 *            the creature
	 */
	public void addCreature(Creature creature) {

		if(creature != null && creature instanceof Player) {
			
			for(int y = creature.getY() - 5; y <= creature.getY() + 5; y++){
				for(int x = creature.getX() - 6; x <= creature.getX() + 6; x++){
					if(x < WIDTH && x >= 0 && y < HEIGHT && y >= 0) {
						fow[y][x] = 'x';
					}
				}
			}
		}
		
		if (creature != null) {
			map[creature.getY()][creature.getX()] = creature.SYMBOL;
		}
	}

	/**
	 * get the map array
	 * 
	 * @return the map
	 */
	public char[][] getMap() {
		 return map;
	}
	
	/**
	 * get the map with fog of war added as an 2d-array
	 * 
	 * @return the map with fog of war
	 */
	public char[][] getVisibleMap() {
		
		for(int y = 0; y < HEIGHT; y++) {
			for(int x = 0; x < WIDTH; x++){
				if(fow[y][x] == 'x') {
					visibleMap[y][x] = map[y][x];
				} else {
					visibleMap[y][x] = ':';
				}
			}
		}

		return visibleMap;
	}

	/**
	 * get which area that is loaded
	 * 
	 * @return the area
	 */
	public int getArea() {
		return area;
	}
}