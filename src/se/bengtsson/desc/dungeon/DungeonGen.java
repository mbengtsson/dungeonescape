package se.bengtsson.desc.dungeon;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Random;

/**
 * This is a random dungeon generator made as a singleton
 * 
 * @author Marcus Bengtsson
 * 
 */
public class DungeonGen implements Serializable {

	private static final long serialVersionUID = 138213374920880093L;
	private final int WIDTH;
	private final int HEIGHT;

	private char[][] dungeon;

	private static DungeonGen instance = new DungeonGen();

	/**
	 * Constructor, sets the dungeons dimensions and creates 
	 * a 2d char array to hold the dungeon
	 */
	private DungeonGen() {
		WIDTH = 58;
		HEIGHT = 27;

		dungeon = new char[HEIGHT][WIDTH];

	}

	/**
	 * Allows other classes to access this class
	 * 
	 * @return instance of DungeonGen
	 */
	public static DungeonGen getInstance() {
		return instance;
	}

	/**
	 * Generates a new random dungeon, divides the map in 16 random cells, then
	 * creates a room in each cell, makes corridors between rooms and finally
	 * creates an exit.
	 * 
	 */
	public void genDungeon() {

		HashMap<String, int[]> cells = new HashMap<String, int[]>();

		fillDungeon();

		cells = makeCells();

		makeRooms(cells);

		makeCorridors(cells);

		makeExit();
	}

	/**
	 * Returns a map of the current dungeon
	 * 
	 * @return map as a char array
	 */
	public char[][] getDungeon() {

		return dungeon;
	}

	/**
	 * Creates an exit on the map and connects it to the rest of the rooms
	 */
	private void makeExit() {

		Random rnd = new Random();

		int exitX = 0;
		int exitY = 0;

		int direction = rnd.nextInt(4);

		if (direction == 0) {
			exitX = rnd.nextInt(WIDTH - 4) + 2;
			exitY = 0;

			for (int y = exitY + 1; y < HEIGHT; y++) {

				if (dungeon[y][exitX] == ' ' || dungeon[y - 1][exitX - 1] == ' ' || dungeon[y - 1][exitX + 1] == ' ') {
					break;
				} else {
					dungeon[y][exitX] = ' ';
				}
			}

		} else if (direction == 1) {
			exitX = rnd.nextInt(WIDTH - 4) + 2;
			exitY = HEIGHT - 1;

			for (int y = exitY - 1; y >= 0; y--) {

				if (dungeon[y][exitX] == ' ' || dungeon[y + 1][exitX - 1] == ' ' || dungeon[y + 1][exitX + 1] == ' ') {
					break;
				} else {
					dungeon[y][exitX] = ' ';
				}

			}

		} else if (direction == 2) {
			exitX = 0;
			exitY = rnd.nextInt(HEIGHT - 4) + 2;

			for (int x = exitX + 1; x < HEIGHT; x++) {

				if (dungeon[exitY][x] == ' ' || dungeon[exitY - 1][x - 1] == ' ' || dungeon[exitY + 1][x - 1] == ' ') {
					break;
				} else {
					dungeon[exitY][x] = ' ';
				}
			}

		} else if (direction == 3) {
			exitX = WIDTH - 1;
			exitY = rnd.nextInt(HEIGHT - 4) + 2;

			for (int x = exitX - 1; x >= 0; x--) {

				if (dungeon[exitY][x] == ' ' || dungeon[exitY - 1][x + 1] == ' ' || dungeon[exitY + 1][x + 1] == ' ') {
					break;
				} else {
					dungeon[exitY][x] = ' ';
				}
			}
		}

		dungeon[exitY][exitX] = 'E';

	}

	/**
	 * Creates corridors between cells in the dungeon
	 * 
	 * @param cells
	 *            a hash-map with all cells
	 */
	private void makeCorridors(HashMap<String, int[]> cells) {

		makeCorridor(cells.get("a"), cells.get("b"));
		makeCorridor(cells.get("c"), cells.get("d"));
		makeCorridor(cells.get("e"), cells.get("f"));
		makeCorridor(cells.get("g"), cells.get("h"));
		makeCorridor(cells.get("i"), cells.get("j"));
		makeCorridor(cells.get("k"), cells.get("l"));
		makeCorridor(cells.get("m"), cells.get("n"));
		makeCorridor(cells.get("o"), cells.get("p"));
		makeCorridor(cells.get("q"), cells.get("r"));
		makeCorridor(cells.get("s"), cells.get("t"));
		makeCorridor(cells.get("u"), cells.get("v"));
		makeCorridor(cells.get("w"), cells.get("x"));
		makeCorridor(cells.get("y"), cells.get("z"));
		makeCorridor(cells.get("A"), cells.get("B"));
		makeCorridor(cells.get("C"), cells.get("D"));

	}

	/**
	 * Creates a corridor between two cells
	 * 
	 * @param cell1
	 *            an array with coordinates for the first cell [x1, y1, x2, y2]
	 *            (*1 is top left and *2 is bottom right)
	 * @param cell2
	 *            an array with coordinates for the second cell [x1, y1, x2, y2]
	 *            (*1 is top left and *2 is bottom right)
	 */
	private void makeCorridor(int[] cell1, int[] cell2) {

		int cell1x = (int) ((cell1[2] + cell1[0]) / 2 - 0.5);
		int cell1y = (int) ((cell1[3] + cell1[1]) / 2 - 0.5);

		int cell2x = (int) ((cell2[2] + cell2[0]) / 2 - 0.5);
		int cell2y = (int) ((cell2[3] + cell2[1]) / 2 - 0.5);

		if (cell1x == cell2x) {
			for (int y = cell1y; y < cell2y; y++) {
				dungeon[y][cell1x] = ' ';
			}
		}

		if (cell1y == cell2y) {
			for (int x = cell1x; x < cell2x; x++) {
				dungeon[cell1y][x] = ' ';
			}
		}

	}

	/**
	 * Creates a room in each cell
	 * 
	 * @param cells
	 *            a hash-map with all cells
	 */
	private void makeRooms(HashMap<String, int[]> cells) {

		makeRoom(cells.get("o"));
		makeRoom(cells.get("p"));
		makeRoom(cells.get("q"));
		makeRoom(cells.get("r"));
		makeRoom(cells.get("s"));
		makeRoom(cells.get("t"));
		makeRoom(cells.get("u"));
		makeRoom(cells.get("v"));
		makeRoom(cells.get("w"));
		makeRoom(cells.get("x"));
		makeRoom(cells.get("y"));
		makeRoom(cells.get("z"));
		makeRoom(cells.get("A"));
		makeRoom(cells.get("B"));
		makeRoom(cells.get("C"));
		makeRoom(cells.get("D"));

	}

	/**
	 * Creates a room in a cell
	 * 
	 * @param bounds
	 *            an array with coordinates for the cell [x1, y1, x2, y2] 
	 *            (*1 is top left and *2 is bottom right)
	 */
	private void makeRoom(int[] bounds) {

		Random rnd = new Random();

		if (bounds == null) {
			return;
		}

		int maxWidth = bounds[2] - bounds[0];
		int maxHeight = bounds[3] - bounds[1];

		int width;
		if (maxWidth < 4) {
			width = maxWidth;
		} else {
			width = rnd.nextInt(maxWidth - 1 - (maxWidth - 2)) + (maxWidth - 2);
		}

		int height;
		if (maxHeight < 4) {
			height = maxHeight;
		} else {
			height = rnd.nextInt(maxHeight - 1 - (maxHeight - 2)) + (maxHeight - 2);
		}

		int xPos = 0;

		if (maxWidth != width) {
			xPos = rnd.nextInt(maxWidth - width);
		}

		int yPos = 0;
		if (maxHeight != height) {
			yPos = rnd.nextInt(maxHeight - height);
		}

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				dungeon[y + bounds[1] + yPos][x + bounds[0] + xPos] = ' ';
			}
		}

	}

	/**
	 * Fills the whole map with '#'
	 */
	private void fillDungeon() {

		for (int y = 0; y < HEIGHT; y++) {
			for (int x = 0; x < WIDTH; x++) {
				dungeon[y][x] = '#';
			}
		}
	}

	/**
	 * Randomly splits an are in two (cells)
	 * 
	 * @param coords
	 *            an array with coordinates for the area [x1, y1, x2, y2]
	 *            (*1 is top left and *2 is bottom right)
	 * @return an 2-dimensional array with the coordinates of the two new areas
	 */
	private int[][] split(int[] coords) {

		Random rnd = new Random();

		int[][] newCoords = new int[2][4];

		int split;

		int topX = coords[0];
		int topY = coords[1];
		int bottomX = coords[2];
		int bottomY = coords[3];

		int width = bottomX - topX;
		int height = bottomY - topY;

		int direction;

		if (width < 11 && height < 6) {
			direction = -1;
		} else if (width < 11) {
			direction = 0;
		} else if (height < 6) {
			direction = 1;
		} else {
			direction = rnd.nextInt(2);
		}

		if (direction == 0) {

			split = rnd.nextInt(height / 3) + height / 3;

			newCoords[0][0] = topX;
			newCoords[0][1] = topY;
			newCoords[0][2] = bottomX;
			newCoords[0][3] = topY + split;
			newCoords[1][0] = topX;
			newCoords[1][1] = topY + split + 1;
			newCoords[1][2] = bottomX;
			newCoords[1][3] = bottomY;

		} else if (direction == 1) {

			split = rnd.nextInt(width / 3) + width / 3;

			newCoords[0][0] = topX;
			newCoords[0][1] = topY;
			newCoords[0][2] = topX + split;
			newCoords[0][3] = bottomY;
			newCoords[1][0] = topX + split + 1;
			newCoords[1][1] = topY;
			newCoords[1][2] = bottomX;
			newCoords[1][3] = bottomY;

		} else {

			newCoords[0][0] = topX;
			newCoords[0][1] = topY;
			newCoords[0][2] = bottomX;
			newCoords[0][3] = bottomY;
			newCoords[1][0] = 0;
			newCoords[1][1] = 0;
			newCoords[1][2] = 0;
			newCoords[1][3] = 0;

		}

		return newCoords;
	}

	/**
	 * Generates 16 random cells to create rooms in
	 * 
	 * @return a hash-map with coordinates for all cells
	 */
	private HashMap<String, int[]> makeCells() {

		HashMap<String, int[]> cells = new HashMap<String, int[]>();

		int[] dungeon = { 1, 1, WIDTH - 1, HEIGHT - 1 };

		cells.put("dungeon", dungeon);
		
		//first split - 2 cells
		int[][] ab = split(dungeon);

		cells.put("a", ab[0]);
		cells.put("b", ab[1]);

		//second split - 4 cells
		int[][] cd = split(ab[0]);
		int[][] ef = split(ab[1]);

		cells.put("c", cd[0]);
		cells.put("d", cd[1]);
		cells.put("e", ef[0]);
		cells.put("f", ef[1]);

		//third split - 8 cells
		int[][] gh = split(cd[0]);
		int[][] ij = split(cd[1]);
		int[][] kl = split(ef[0]);
		int[][] mn = split(ef[1]);

		cells.put("g", gh[0]);
		cells.put("h", gh[1]);
		cells.put("i", ij[0]);
		cells.put("j", ij[1]);
		cells.put("k", kl[0]);
		cells.put("l", kl[1]);
		cells.put("m", mn[0]);
		cells.put("n", mn[1]);

		//fourth split - 16 cells
		int[][] op = split(gh[0]);
		int[][] qr = split(gh[1]);
		int[][] st = split(ij[0]);
		int[][] uv = split(ij[1]);
		int[][] wx = split(kl[0]);
		int[][] yz = split(kl[1]);
		int[][] AB = split(mn[0]);
		int[][] CD = split(mn[1]);

		cells.put("o", op[0]);
		cells.put("p", op[1]);
		cells.put("q", qr[0]);
		cells.put("r", qr[1]);
		cells.put("s", st[0]);
		cells.put("t", st[1]);
		cells.put("u", uv[0]);
		cells.put("v", uv[1]);
		cells.put("w", wx[0]);
		cells.put("x", wx[1]);
		cells.put("y", yz[0]);
		cells.put("z", yz[1]);
		cells.put("A", AB[0]);
		cells.put("B", AB[1]);
		cells.put("C", CD[0]);
		cells.put("D", CD[1]);

		return cells;
	}

}
